﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Git_Patcher
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void selectFolder_Click(object sender, EventArgs e)
		{
			if(folderBrowserDialog1.ShowDialog() == DialogResult.OK)
			{
				selectedFolderTb.Text = folderBrowserDialog1.SelectedPath;
			}
		}

		private void InitGitRepository()
		{
			ProcessStartInfo startInfo = new ProcessStartInfo();
			startInfo.FileName = @"git.exe";
			startInfo.WorkingDirectory = selectedFolderTb.Text;
			startInfo.Arguments = "log --pretty=format:\" /% H / - % an : % s\" -10";
			startInfo.RedirectStandardOutput = true;
			startInfo.UseShellExecute = false;
			Process git = Process.Start(startInfo);
			string log = git.StandardOutput.ReadToEnd();
			git.WaitForExit();
			listBox1.Items.AddRange(log.Split(new Char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries));
		}

		void ShowCommit(string commitID)
		{
			ProcessStartInfo startInfo = new ProcessStartInfo();
			startInfo.FileName = @"git.exe";
			startInfo.WorkingDirectory = selectedFolderTb.Text;
			startInfo.Arguments = "show " + commitID;
			startInfo.RedirectStandardOutput = true;
			startInfo.UseShellExecute = false;
			Process git = Process.Start(startInfo);
			string log = git.StandardOutput.ReadToEnd();

			commit.Text = log;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			InitGitRepository();
		}

		private void listBox1_DoubleClick(object sender, EventArgs e)
		{
			string selectedCommit = listBox1.SelectedItem.ToString();
			System.Text.RegularExpressions.Regex regexp = new System.Text.RegularExpressions.Regex(@"/(.*)/.*");
			Match match = regexp.Match(selectedCommit);
			string commitId = match.Groups[1].Value;
			ShowCommit(commitId);
		}
	}
}
